import 'package:flutter/material.dart';
import 'package:responsive/Screens/homescreen.dart/Footer/footer.dart';
import 'package:responsive/Screens/homescreen.dart/MainBody/main_body.dart';
import 'package:responsive/Screens/homescreen.dart/Navbar/navbar.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LayoutBuilder(builder: (ctx, constraints) {
      if (constraints.maxWidth <= 576) {
        return Scaffold(
            appBar: AppBar(
              shadowColor: Colors.transparent,
              backgroundColor: Color.fromRGBO(19, 51, 58, 1),
              leading: Builder(
                builder: (context) => IconButton(
                  icon: Icon(Icons.hourglass_empty),
                  onPressed: () => Scaffold.of(context).openDrawer(),
                ),
              ),
              title: Text("Burnham"),
            ),
            drawer: LayoutBuilder(builder: (context, constraints) {
              if (constraints.maxWidth <= 576) {
                return ClipRRect(
                    borderRadius:
                        BorderRadius.only(topRight: Radius.circular(50)),
                    child: _appDrawer(context));
              } else {
                return Container(
                  height: 0,
                );
              }
            }),
            body: SingleChildScrollView(
              child: Column(
                children: [NavBar(), MainBody(), Footer()],
              ),
            ));
      } else {
        return Scaffold(
            body: SingleChildScrollView(
          child: Column(
            children: [NavBar(), MainBody(), Footer()],
          ),
        ));
      }
    });
  }

  Widget _appDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Container(
            height: 140,
            alignment: Alignment.center,
            child: DrawerHeader(
              padding: EdgeInsets.all(40),
              child: Text("Pick Your Poison",
                  style: TextStyle(
                    fontSize: 20,
                  )),
            ),
          ),
          Divider(color: Colors.black),
          ListTile(
            title: Center(child: Text("Who We Are")),
            onTap: () {},
          ),
          Divider(color: Colors.black),
          ListTile(
            title: Center(child: Text("What We Do")),
            onTap: () {},
          ),
          Divider(color: Colors.black),
          ListTile(
            title: Center(child: Text("ACA")),
            onTap: () {},
          ),
          Divider(color: Colors.black),
          ListTile(
            title: Center(child: Text("Contact Us")),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}

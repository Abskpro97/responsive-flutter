import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth > 576) {
        return Container(
          color: Color.fromRGBO(19, 51, 58, 1),
          width: double.infinity,
          height: size.height * 0.3,
          child: _large(context),
        );
      } else {
        return Container(
          color: Color.fromRGBO(19, 51, 58, 1),
          width: double.infinity,
          height: size.height * 0.5,
          child: _tiny(context),
        );
      }
    });
  }

  Widget _large(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 50, horizontal: 90),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Product",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Text("Feature", style: TextStyle(color: Colors.white)),
                    Text("Security", style: TextStyle(color: Colors.white)),
                    Text("Team", style: TextStyle(color: Colors.white)),
                    Text("Enterprise", style: TextStyle(color: Colors.white)),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Platform",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Text("Developer Api",
                        style: TextStyle(color: Colors.white)),
                    Text("Partners", style: TextStyle(color: Colors.white)),
                    Text("Atom", style: TextStyle(color: Colors.white)),
                    Text("Electron", style: TextStyle(color: Colors.white)),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Support",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Text("Docs", style: TextStyle(color: Colors.white)),
                    Text("Community Forum",
                        style: TextStyle(color: Colors.white)),
                    Text("Status", style: TextStyle(color: Colors.white)),
                    Text("Contact", style: TextStyle(color: Colors.white)),
                    Text("Service", style: TextStyle(color: Colors.white)),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            width: 300,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("2021 \u00a9' Burnham inc",
                    style: TextStyle(color: Colors.white)),
                Text("Terms", style: TextStyle(color: Colors.white)),
                Text("Privacy", style: TextStyle(color: Colors.white)),
                Text("Site Map", style: TextStyle(color: Colors.white))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _tiny(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 90),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Product",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Text("Feature", style: TextStyle(color: Colors.white)),
                    Text("Security", style: TextStyle(color: Colors.white)),
                    Text("Team", style: TextStyle(color: Colors.white)),
                    Text("Enterprise", style: TextStyle(color: Colors.white)),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Platform",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Text("Developer Api",
                        style: TextStyle(color: Colors.white)),
                    Text("Partners", style: TextStyle(color: Colors.white)),
                    Text("Atom", style: TextStyle(color: Colors.white)),
                    Text("Electron", style: TextStyle(color: Colors.white)),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("Support",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                Text("Docs", style: TextStyle(color: Colors.white)),
                Text("Community Forum", style: TextStyle(color: Colors.white)),
                Text("Status", style: TextStyle(color: Colors.white)),
                Text("Contact", style: TextStyle(color: Colors.white)),
                Text("Service", style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text("2018 \u00a9' Burnham inc",
                      style: TextStyle(color: Colors.white)),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text("Terms", style: TextStyle(color: Colors.white)),
                  Text("Privacy", style: TextStyle(color: Colors.white)),
                  Text("Site Map", style: TextStyle(color: Colors.white))
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}

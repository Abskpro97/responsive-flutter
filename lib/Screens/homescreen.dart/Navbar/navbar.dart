import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class NavBar extends StatefulWidget {
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth >= 992) {
        return Container(
          width: double.infinity,
          alignment: Alignment.topCenter,
          height: size.height,
          color: Color.fromRGBO(19, 51, 58, 1),
          child: _large(context),
        );
      } else if (constraints.maxWidth > 768 && constraints.maxWidth <= 991) {
        return Container(
          width: double.infinity,
          alignment: Alignment.topCenter,
          height: size.height * 0.65,
          color: Color.fromRGBO(19, 51, 58, 1),
          child: _medium(context),
        );
      } else if (constraints.maxWidth > 576 && constraints.maxWidth <= 768) {
        return Container(
          width: double.infinity,
          alignment: Alignment.topCenter,
          height: size.height * 0.7,
          color: Color.fromRGBO(19, 51, 58, 1),
          child: _small(context),
        );
      } else {
        return Container(
          width: double.infinity,
          alignment: Alignment.topCenter,
          height: size.height * 0.65,
          color: Color.fromRGBO(19, 51, 58, 1),
          child: _tiny(context),
        );
      }
    });
  }

  Widget _large(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 40, left: 90, right: 90),
      child: Column(
        children: [
          Container(
            height: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Row(
                    children: [
                      Icon(Icons.hourglass_empty,
                          size: 50, color: Colors.white),
                      Text("Burnham",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 40,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                )),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Who We Are",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                      Text("What We Do",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                      Text("ACA",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                      Container(
                          padding: EdgeInsets.all(18),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.white)),
                          child: Text("Contact Us",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20)))
                    ],
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                      Text("Real benefits.",
                          style: TextStyle(
                              fontSize: 70,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      Text("Real People.",
                          style: TextStyle(
                              fontSize: 70,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      Text("We combine corporate benifits with strategic",
                          style: TextStyle(fontSize: 20, color: Colors.white)),
                      Text("financial management to provide valuable",
                          style: TextStyle(fontSize: 20, color: Colors.white)),
                      Text("solution for companies at scale",
                          style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        padding: EdgeInsets.all(18),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          "Schedule a Consult",
                          style: TextStyle(fontSize: 25),
                        ),
                      )
                    ])),
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Image.asset("images/banner.jpeg", fit: BoxFit.cover),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _medium(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 30, horizontal: 60),
        child: Column(children: [
          Container(
            height: 120,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.only(left: 6),
                  child: Row(
                    children: [
                      Icon(Icons.hourglass_empty,
                          size: 40, color: Colors.white),
                      Text("Burnham",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                )),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Who We Are",
                          style: TextStyle(color: Colors.white, fontSize: 14)),
                      Text("What We Do",
                          style: TextStyle(color: Colors.white, fontSize: 14)),
                      Text("ACA",
                          style: TextStyle(color: Colors.white, fontSize: 14)),
                      Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: Colors.white)),
                          child: Text("Contact Us",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)))
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                      Text("Real benefits.",
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      Text("Real People.",
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      Text("We combine corporate benifits with strategic",
                          style: TextStyle(fontSize: 14, color: Colors.white)),
                      Text("financial management to provide valuable",
                          style: TextStyle(fontSize: 14, color: Colors.white)),
                      Text("solution for companies at scale",
                          style: TextStyle(fontSize: 14, color: Colors.white)),
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8)),
                        child: Text(
                          "Schedule a Consult",
                          style: TextStyle(fontSize: 18),
                        ),
                      )
                    ])),
                Expanded(
                  // flex: 2,
                  child: Container(
                    child: Image.asset("images/banner.jpeg", fit: BoxFit.cover),
                  ),
                )
              ],
            ),
          )
        ]));
  }

  Widget _small(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 100,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.only(left: 3),
                  child: Row(
                    children: [
                      Icon(Icons.hourglass_empty,
                          color: Colors.white, size: 30),
                      Text("Burnham",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)),
                    ],
                  ),
                )),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Who We Are",
                          style: TextStyle(color: Colors.white, fontSize: 10)),
                      Text("What We Do",
                          style: TextStyle(color: Colors.white, fontSize: 10)),
                      Text("ACA",
                          style: TextStyle(color: Colors.white, fontSize: 10)),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: Colors.white)),
                          child: Text("Contact Us",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10)))
                    ],
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                      Text("Real benefits.",
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      Text("Real People.",
                          style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      SizedBox(
                        height: 5,
                      ),
                      Text("We combine corporate benifits with strategic",
                          style: TextStyle(fontSize: 10, color: Colors.white)),
                      Text("financial management to provide valuable",
                          style: TextStyle(fontSize: 10, color: Colors.white)),
                      Text("solution for companies at scale",
                          style: TextStyle(fontSize: 10, color: Colors.white)),
                      SizedBox(
                        height: 18,
                      ),
                      Container(
                        padding: EdgeInsets.all(9),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          "Schedule a Consult",
                          style: TextStyle(fontSize: 14),
                        ),
                      )
                    ])),
                SizedBox(
                  height: 5,
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    child:
                        Image.asset("images/banner.jpeg", fit: BoxFit.contain),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget _tiny(BuildContext context) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 10),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Column(
            children: [
              Expanded(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                    Text("Real benefits.",
                        style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    Text("Real People.",
                        style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    SizedBox(
                      height: 5,
                    ),
                    Text("We combine corporate benifits with strategic",
                        style: TextStyle(fontSize: 10, color: Colors.white)),
                    Text("financial management to provide valuable",
                        style: TextStyle(fontSize: 10, color: Colors.white)),
                    Text("solution for companies at scale",
                        style: TextStyle(fontSize: 10, color: Colors.white)),
                    SizedBox(
                      height: 18,
                    ),
                    Container(
                      padding: EdgeInsets.all(9),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5)),
                      child: Text(
                        "Schedule a Consult",
                        style: TextStyle(fontSize: 14),
                      ),
                    )
                  ])),
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Image.asset("images/banner.jpeg", fit: BoxFit.contain),
                ),
              )
            ],
          ),
        )
      ],
    ),
  );
}

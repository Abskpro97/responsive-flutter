import 'package:flutter/material.dart';
import 'package:responsive/Screens/homescreen.dart/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Color color = Color.fromRGBO(19, 51, 58, 1);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: SafeArea(child: HomeScreen()),
    );
  }
}
